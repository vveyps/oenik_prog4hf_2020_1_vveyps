﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PhoneShop.ConsoleClient
{
    public class Brand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Forgalom { get; set; }
        public string Csakinf { get; set; }
        public string Szoftver { get; set; }
        public string Weblap { get; set; }
        public override string ToString()
        {
            return $"Id={Id}\tBrandName={Name}\tForgalom={Forgalom}\tCsakInfo={Csakinf}\tSzoftver={Szoftver}\tWeblap={Weblap}";
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting...");
            Console.ReadLine();
            string url = "http://localhost:55078/api/BrandsApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list = JsonConvert.DeserializeObject<List<Brand>>(json);
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
                Console.ReadLine();

                Dictionary<string,string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(Brand.Name), "OnePlus");
                postData.Add(nameof(Brand.Forgalom), "900000");
                postData.Add(nameof(Brand.Csakinf), "True");
                postData.Add(nameof(Brand.Szoftver), "Android");
                postData.Add(nameof(Brand.Weblap), "www.oneplus.com");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("ADD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                int brandId = JsonConvert.DeserializeObject<List<Brand>>(json).Single(x => x.Name == "OnePlus").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(Brand.Id), brandId.ToString());
                postData.Add(nameof(Brand.Name), "OnePlus");
                postData.Add(nameof(Brand.Forgalom), "1200000");
                postData.Add(nameof(Brand.Csakinf), "True");
                postData.Add(nameof(Brand.Szoftver), "Android");
                postData.Add(nameof(Brand.Weblap), "www.oneplus.com");
                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("MOD: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();

                json = client.GetStringAsync(url + "del/" + brandId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("DEL: " + response);
                Console.WriteLine("ALL: " + json);
                Console.ReadLine();
            }
        }
    }
}
