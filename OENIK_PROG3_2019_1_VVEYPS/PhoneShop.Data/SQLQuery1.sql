﻿
CREATE TABLE Gyártó(
Gyártó_ID numeric NOT NULL,
Gyártó_Név varchar(30) NOT NULL,
Éves_forgalom numeric,
Csak_info varchar(20),
Szoftver varchar(20),
Weblap varchar(30),
CONSTRAINT gyártó_pk PRIMARY KEY(Gyártó_ID));
CREATE TABLE Telefonok(
Modell_Név varchar(30) NOT NULL,
 Kiadás_éve numeric,
 Kamera numeric,
 Operációs_rendszer varchar(30) NOT NULL,
 Ram numeric NOT NULL,
 Tárhely numeric NOT NULL,
Gyártó_ID numeric NOT NULL,
CONSTRAINT telefonok_pk PRIMARY KEY(Modell_Név),
CONSTRAINT telefon_fk FOREIGN KEY(Gyártó_ID) REFERENCES Gyártó(Gyártó_ID));
CREATE TABLE Székhely(
Város varchar(30),
Székhely_ID numeric NOT NULL,
 Gyártó_ID numeric NOT NULL,
Ország varchar(30),
Kontinens varchar(30),
Monopol varchar(20),
Székhely_típus varchar(30),
 CONSTRAINT Székhely_pk PRIMARY KEY(Székhely_ID),
 CONSTRAINT székhely_fk1 FOREIGN KEY(Gyártó_ID) REFERENCES Gyártó(Gyártó_ID));
INSERT INTO GYÁRTÓ VALUES(1,'Sony', 315000000, 'false', 'Android', 'www.sony.com');
INSERT INTO GYÁRTÓ VALUES(2,'Samsung', 815000000, 'false', 'Android', 'www.samsung.com');
INSERT INTO GYÁRTÓ VALUES(3,'Apple', 615000000, 'true', 'ios', 'www.apple.com');
INSERT INTO GYÁRTÓ VALUES(4,'Huawei', 915000000, 'false', 'emui', 'www.huawei.com');
INSERT INTO GYÁRTÓ VALUES(5,'Honor', 215000000, 'true', 'emui', 'www.honor.com');
INSERT INTO GYÁRTÓ VALUES(6,'LG', 315000000, 'false', 'Android', 'www.lg.com');
INSERT INTO GYÁRTÓ VALUES(7,'Nokia', 365000000, 'false', 'windows', 'www.nokia.com');
INSERT INTO GYÁRTÓ VALUES(8,'Alcatel', 115000000, 'false', 'Android', 'www.alcatel.com');
INSERT INTO GYÁRTÓ VALUES(9,'BlackBerry', 280000000, 'true', 'Android', 'www.blackberry.com');
INSERT INTO GYÁRTÓ VALUES(10,'HTC', 25000000, 'false', 'Android', 'www.htc.com');
INSERT INTO GYÁRTÓ VALUES(11,'Xiaomi', 35000000, 'false', 'Android', 'www.xiaomi.com');
INSERT INTO GYÁRTÓ VALUES(12,'Lenovo', 55000000, 'false', 'Android', 'www.lenovo.com');
INSERT INTO GYÁRTÓ VALUES(13,'Caterpillar', 45000000, 'false', 'Android', 'www.caterpillar.com');
INSERT INTO GYÁRTÓ VALUES(14,'Asus', 75000000, 'false', 'Android', 'www.asus.com');
INSERT INTO GYÁRTÓ VALUES(15,'Meizu', 29000000, 'false', 'Android', 'www.meizu.com');


INSERT INTO Telefonok VALUES('Xperia XZ3', 2018,19,'Android',4,64,1);
INSERT INTO Telefonok VALUES('Xperia XA2',2018,23,'Android',3,32,1);
INSERT INTO Telefonok VALUES('Xperia XZ2 COMPACT',2018,19,'Android',4,64,1);
INSERT INTO Telefonok VALUES('Xperia XZ1',2017,19,'Android',4,64,1);
INSERT INTO Telefonok VALUES('Xperia XZ4',2017,13,'Android',2,16,1);
INSERT INTO Telefonok VALUES('Galaxy S9',2018,12,'Android',4,64,2);
INSERT INTO Telefonok VALUES('Galaxy S9+',2018,12,'Android',6,64,2);
INSERT INTO Telefonok VALUES('Galaxy NOTE 9',2018,12,'Android',6,128,2);
INSERT INTO Telefonok VALUES('Galaxy S8',2017,12,'Android',4,64,2);
INSERT INTO Telefonok VALUES('Galaxy j6',2018,13,'Android',3,32,2);
INSERT INTO Telefonok VALUES('Galaxy S7',2016,12,'Android',4,32,2);
INSERT INTO Telefonok VALUES('Iphone X',2017,12,'IOS',3,64,3);
INSERT INTO Telefonok VALUES('Iphone XS',2018,12,'IOS',4,64,3);
INSERT INTO Telefonok VALUES('Iphone XS MAX',2018,12,'IOS',4,512,3);
INSERT INTO Telefonok VALUES('Iphone 8',2017,12,'IOS',2,64,3);
INSERT INTO Telefonok VALUES('Iphone 7',2016,12,'IOS',2,32,3);
INSERT INTO Telefonok VALUES('P20 Pro',2018,40,'Android',6,128,4);
INSERT INTO Telefonok VALUES('P20',2018,24,'Android',4,64,4);
INSERT INTO Telefonok VALUES('MATE 20 PRO',2018,40,'Android',6,128,4);
INSERT INTO Telefonok VALUES('Y5 prime',2018,8,'Android',2,16,4);
INSERT INTO Telefonok VALUES('6',2014,13,'Android',3,16,5);
INSERT INTO Telefonok VALUES('10',2018,24,'Android',4,64,5);
INSERT INTO Telefonok VALUES('PLAY',2018,16,'Android',4,64,5);
INSERT INTO Telefonok VALUES('8',2016,12,'Android',4,32,5);
INSERT INTO Telefonok VALUES('G7 ThinkQ',2018,16,'Android',4,64,6);
INSERT INTO Telefonok VALUES('G6',2017,13,'Android',4,32,6);
INSERT INTO Telefonok VALUES('V 30',2018,16,'Android',4,64,6);
INSERT INTO Telefonok VALUES('K4',2017,5,'Android',1,8,6);
INSERT INTO Telefonok VALUES('7 Plus',2018,13,'Android',4,64,7);
INSERT INTO Telefonok VALUES('5',2017,13,'Android',2,16,7);
INSERT INTO Telefonok VALUES('5 V',2018,14,'Android',3,32,8);
INSERT INTO Telefonok VALUES('KEY one',2017,12,'Android',3,32,9);
INSERT INTO Telefonok VALUES('U 12+',2018,12,'Android',6,64,10);
INSERT INTO Telefonok VALUES('Mi 8',2018,12,'Android',6,128,11);
INSERT INTO Telefonok VALUES('Redmi 6',2018,12,'Android',3,32,11);
INSERT INTO Telefonok VALUES('K 6',2016,13,'Android',2,16,12);
INSERT INTO Telefonok VALUES('S60',2016,13,'Android',3,32,13);
INSERT INTO Telefonok VALUES('ZenFone 5',2018,12,'Android',4,64,14);
INSERT INTO Telefonok VALUES('Pro 6 Plus',2016,12,'Android',4,64,15);

INSERT INTO Székhely VALUES('Tokió', 1,1, 'Japán', 'Ázsia', 'false', 'main');
INSERT INTO Székhely VALUES('Szöul', 2,2, 'Dél-Korea', 'Ázsia', 'false', 'main');
INSERT INTO Székhely VALUES('Cupertino', 3,3, 'USA', 'Észak-Amerika', 'false', 'main');
INSERT INTO Székhely VALUES('Kuangtung', 4,4, 'Kína', 'Ázsia', 'false', 'main');
INSERT INTO Székhely VALUES('Kuangtung', 5,5, 'Kína', 'Ázsia', 'false', 'main');
INSERT INTO Székhely VALUES('Szöul', 6,6, 'Dél-Korea', 'Ázsia', 'false', 'main');
INSERT INTO Székhely VALUES('Espoo', 7,7, 'Finnország', 'Európa', 'true', 'main');
INSERT INTO Székhely VALUES('Sunnyvale', 8,8, 'USA', 'Észak-Amerika', 'false', 'main');
INSERT INTO Székhely VALUES('Newyork', 9,9, 'USA', 'Észak-Amerika', 'false', 'main');
INSERT INTO Székhely VALUES('Tajvan', 10,10, 'Tajvan', 'Ázsia', 'false', 'main');
INSERT INTO Székhely VALUES('Peking', 11,11, 'Kína', 'Ázsia', 'false', 'main');
INSERT INTO Székhely VALUES('Peking', 12,12, 'Kína', 'Ázsia', 'false', 'main');
INSERT INTO Székhely VALUES('Peoria', 13,13, 'USA', 'Észak-Amerika', 'false', 'main');
INSERT INTO Székhely VALUES('Tajvan', 14,14, 'Tajvan', 'Ázsia', 'false', 'main');
INSERT INTO Székhely VALUES('Zhuhai', 15,15, 'Kína', 'Ázsia', 'false', 'main');

