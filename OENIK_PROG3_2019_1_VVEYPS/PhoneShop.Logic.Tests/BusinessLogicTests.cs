﻿// <copyright file="BusinessLogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using NUnit.Framework;
    using PhoneShop.Data;
    using PhoneShop.Repository;

    /// <summary>
    /// Az üzleti logika tesztelése.
    /// </summary>
    [TestFixture]
    public class BusinessLogicTests
    {
        private Mock<IRepository<Telefonok>> teloteszt;
        private Mock<IRepository<Gyártó>> gyartoteszt;
        private Mock<IRepository<Székhely>> szekhelyteszt;
        private Mock<BusinessLogic> logic;

        /// <summary>
        /// A mockolt reposytorik setupja.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.logic = new Mock<BusinessLogic>();
            this.teloteszt = new Mock<IRepository<Telefonok>>();
            this.gyartoteszt = new Mock<IRepository<Gyártó>>();
            this.szekhelyteszt = new Mock<IRepository<Székhely>>();
            List<Telefonok> telok = new List<Telefonok>()
            {
                new Telefonok { Gyártó_ID = 1, Kamera = 12, Kiadás_éve = 2012, Modell_Név = "Iphone 6", Operációs_rendszer = "IOS", Ram = 2, Tárhely = 128 },
                new Telefonok { Gyártó_ID = 2, Kamera = 8, Kiadás_éve = 2014, Modell_Név = "G4", Operációs_rendszer = "Android", Ram = 1, Tárhely = 32 },
                new Telefonok { Gyártó_ID = 3, Kamera = 16, Kiadás_éve = 2016, Modell_Név = "Galaxy S7", Operációs_rendszer = "Android", Ram = 2, Tárhely = 64 }
            };
            List<Gyártó> gyartok = new List<Gyártó>()
            {
                new Gyártó { Gyártó_ID = 1, Gyártó_Név = "Apple", Szoftver = "IOS", Csak_info = "True", Weblap = "www.apple.com", Éves_forgalom = 13200000 },
                new Gyártó { Gyártó_ID = 2, Gyártó_Név = "LG", Szoftver = "Android", Csak_info = "False", Weblap = "www.lg.com", Éves_forgalom = 9810000 },
                new Gyártó { Gyártó_ID = 3, Gyártó_Név = "Samsung", Szoftver = "Android", Csak_info = "False", Weblap = "www.samsung.com", Éves_forgalom = 43200000 }
            };
            List<Székhely> szekhelyek = new List<Székhely>()
            {
                new Székhely { Gyártó_ID = 1, Kontinens = "Észak-Amerika", Monopol = "False", Ország = "USA", Székhely_ID = 1, Székhely_típus = "Fő", Város = "Cupertino" },
                new Székhely { Gyártó_ID = 2, Kontinens = "Ázsia", Monopol = "False", Ország = "Dél-Korea", Székhely_ID = 2, Székhely_típus = "Fő", Város = "Szöul" },
                new Székhely { Gyártó_ID = 3, Kontinens = "Ázsia", Monopol = "False", Ország = "Dél-Korea", Székhely_ID = 3, Székhely_típus = "Fő", Város = "Szöul" }
            };
            this.teloteszt.Setup(x => x.Read()).Returns(telok);
            this.gyartoteszt.Setup(x => x.Read()).Returns(gyartok);
            this.szekhelyteszt.Setup(x => x.Read()).Returns(szekhelyek);

            this.teloteszt.Setup(x => x.Create(It.IsAny<Telefonok>())).Callback((Telefonok telefon) => telok.Add(telefon));
            this.gyartoteszt.Setup(x => x.Create(It.IsAny<Gyártó>())).Callback((Gyártó gyarto) => gyartok.Add(gyarto));
            this.szekhelyteszt.Setup(x => x.Create(It.IsAny<Székhely>())).Callback((Székhely szek) => szekhelyek.Add(szek));

            this.teloteszt.Setup(x => x.Delete(It.IsAny<Telefonok>())).Callback((Telefonok telo) => telok.Remove(telo));
            this.gyartoteszt.Setup(x => x.Delete(It.IsAny<Gyártó>())).Callback((Gyártó gy) => gyartok.Remove(gy));
            this.szekhelyteszt.Setup(x => x.Delete(It.IsAny<Székhely>())).Callback((Székhely sz) => szekhelyek.Remove(sz));

            this.teloteszt.Setup(x => x.GetByID(It.IsAny<string>())).Returns((string id) => telok.Find(x => x.Modell_Név == id));
            this.gyartoteszt.Setup(x => x.GetByID(It.IsAny<string>())).Returns((string id) => gyartok.Find(x => x.Gyártó_ID == int.Parse(id)));
            this.szekhelyteszt.Setup(x => x.GetByID(It.IsAny<string>())).Returns((string id) => szekhelyek.Find(x => x.Székhely_ID == int.Parse(id)));
        }

        /// <summary>
        /// A telefonok olvasásának tesztelése.
        /// </summary>
        [Test]
        public void ItisReadingAllThePhones()
        {
            Assert.That(this.logic.Object.ReadTelok(this.teloteszt.Object).Count, Is.EqualTo(3));
        }

        /// <summary>
        /// Teszt arra, hogy megfelelő gyártót ad e vissza a GetGyartok metódus a megadott id alapján.
        /// </summary>
        [Test]
        public void GetsTheCorrectGyartoById()
        {
            Assert.That(this.logic.Object.GetGyartok(this.gyartoteszt.Object, "1").Gyártó_Név, Is.EqualTo("Apple"));
        }

        /// <summary>
        /// Teszt arra, hogy megfelelő telefont ad e vissza a GetTelok metódus a megadott id alapján.
        /// </summary>
        [Test]
        public void GetsTheCorrectPhoneByID()
        {
            Assert.That(this.logic.Object.GetTelok(this.teloteszt.Object, "G4").Modell_Név, Is.EqualTo("G4"));
        }

        /// <summary>
        /// Az update metódus tesztelése a székhely táblán.
        /// </summary>
        [Test]
        public void SzekhelyVarosUpdatedSuccesfully()
        {
            Székhely sz = this.logic.Object.GetSzekhely(this.szekhelyteszt.Object, "1");
            Székhely updated = sz;
            updated.Város = "NewYork";
            this.logic.Object.UpdateSzekhely(this.szekhelyteszt.Object, sz, updated);

            Assert.That(this.logic.Object.GetSzekhely(this.szekhelyteszt.Object, "1").Város, Is.EqualTo("NewYork"));
        }

        /// <summary>
        /// A create metódus tesztelése a telefonok táblán.
        /// </summary>
        [Test]
        public void AddingTelo()
        {
            Telefonok t = new Telefonok { Gyártó_ID = 3, Kamera = 24, Kiadás_éve = 2019, Modell_Név = "S10", Operációs_rendszer = "Android", Ram = 6, Tárhely = 320 };
            this.logic.Object.AddTelefon(this.teloteszt.Object, t);
            Assert.That(this.teloteszt.Object.Read().Count, Is.EqualTo(4));
        }

        /// <summary>
        /// A delete metódus tesztelése a telefonok táblán.
        /// </summary>
        [Test]
        public void DeleteTelo()
        {
            Telefonok t = new Telefonok { Gyártó_ID = 3, Kamera = 24, Kiadás_éve = 2019, Modell_Név = "S10", Operációs_rendszer = "Android", Ram = 6, Tárhely = 320 };
            this.logic.Object.AddTelefon(this.teloteszt.Object, t);
            this.logic.Object.Delete(TableNames.Telefonok, "S10", this.teloteszt.Object, null, null);
            Assert.That(this.logic.Object.ReadTelok(this.teloteszt.Object).Count, Is.EqualTo(3));
        }

        /// <summary>
        /// A delete metódus tesztelése a székhely táblán.
        /// </summary>
        [Test]
        public void DeleteSzekhely()
        {
            this.logic.Object.Delete(TableNames.Székhely, "1", null, null, this.szekhelyteszt.Object);
            Assert.That(this.logic.Object.SzekhelyRead(this.szekhelyteszt.Object).Count, Is.EqualTo(2));
        }

        /// <summary>
        /// A delete metódus tesztelése a gyártók táblán.
        /// </summary>
        [Test]
        public void DeleteGyarto()
        {
            this.logic.Object.Delete(TableNames.Gyártó, "1", null, this.gyartoteszt.Object, null);
            Assert.That(this.logic.Object.ReadGyartok(this.gyartoteszt.Object).Count, Is.EqualTo(2));
        }

        /// <summary>
        /// Adott gáyrtó telefonjainak lekérdezésének tesztelése.
        /// </summary>
        [Test]
        public void AdottGyartoTelefonjaiTest()
        {
            Assert.That(this.logic.Object.AdottGyartoTelefonjai(this.teloteszt.Object, 2).Count, Is.EqualTo(1));
        }

        /// <summary>
        /// Adott évnél később megjelent telefonok lekérdezésének tesztelése.
        /// </summary>
        [Test]
        public void AdottEvnelKesobbiTelokTest()
        {
            Assert.That(this.logic.Object.AdottEvnelKesobbiTelok(this.teloteszt.Object, 2015), Is.EqualTo("Galaxy S7" + "\n"));
        }

        /// <summary>
        /// Adott gyártó országának és városának lekérdezésének tesztelése.
        /// </summary>
        [Test]
        public void AdottGyartoOrszagaEsVarosa()
        {
            Assert.That(this.logic.Object.AdottGyartoOrszagVaros(this.szekhelyteszt.Object, 2), Is.EqualTo("Dél-Korea" + " " + "Szöul"));
        }
    }
}
