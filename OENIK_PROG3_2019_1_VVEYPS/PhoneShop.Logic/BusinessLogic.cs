﻿// <copyright file="BusinessLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhoneShop.Data;
    using PhoneShop.Repository;

    /// <summary>
    /// A táblák neveit tároló enum.
    /// </summary>
    public enum TableNames
    {
        /// <summary>
        /// A gyártók táblájának neve.
        /// </summary>
        Gyártó,

        /// <summary>
        /// A telefonok táblájának neve.
        /// </summary>
        Telefonok,

        /// <summary>
        /// A tszékhelyek táblájának neve.
        /// </summary>
        Székhely
    }

    /// <summary>
    /// Az üzleti logika osztálya.
    /// </summary>
    public class BusinessLogic : ILogic
    {
        private readonly TelefonRepository tRepo;
        private readonly GyártóRepository gyRepo;
        private readonly SzékhelyRepository szRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
        /// Publikos konstruktor az üzleti logika osztályhoz.
        /// </summary>
        public BusinessLogic()
        {
            this.tRepo = new TelefonRepository();
            this.gyRepo = new GyártóRepository();
            this.szRepo = new SzékhelyRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BusinessLogic"/> class.
        /// Gyártó hozzáadásához szükséges metódus.
        /// </summary>
        /// <param name="gyrepo">A metódushoz szükséges gyártó repository.</param>
        public BusinessLogic(GyártóRepository gyrepo)
        {
            this.gyRepo = gyrepo;
        }

        /// <summary>
        /// Gyártó hozzáadásához szükséges metódus.
        /// </summary>
        /// <param name="gyartorepo">A metódushoz szükséges gyártó repository.</param>
        /// <param name="gyarto">A hozzáadni kívánt gyártó példány.</param>
        public void AddGyarto(IRepository<Gyártó> gyartorepo, Gyártó gyarto)
        {
            try
            {
                gyartorepo.Create(gyarto);
            }
            catch (Exception)
            {
                Console.Clear();
                Console.WriteLine("A hozzáadást nem sikerült elvégezni, a megadott kulcs már foglalt vagy nem megfelelő formátumú!");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Hozzáadás metódus.
        /// </summary>
        /// <param name="name">A hozzáadni kívánt elem neve.</param>
        /// <param name="forg">A hozzáadni kívánt elem éves forgalma.</param>
        /// <param name="csakinf">A hozzáadni kívánt elem csakinf adattagja.</param>
        /// <param name="szoftver">A hozzáadni kívánt elem szoftvere.</param>
        /// <param name="weblap">A hozzáadni kívánt elem weblapja.</param>
        public void AddGyartoForProg4(string name, int forg, string csakinf, string szoftver, string weblap)
        {
            int id = int.Parse(this.gyRepo.Read().Max(x => x.Gyártó_ID).ToString()) + 1;

                    Gyártó add = new Gyártó();

                    add.Gyártó_ID = id;
                    add.Gyártó_Név = name;
                    add.Szoftver = szoftver;
                    add.Weblap = weblap;
                    add.Éves_forgalom = forg;
                    add.Csak_info = csakinf;
                    this.gyRepo.Createforprog4(add);
        }

        /// <summary>
        /// Székhely hozzáadásához szükséges metódus.
        /// </summary>
        /// <param name="szekhelyrepo">A metódushoz szükséges székhely repository.</param>
        /// <param name="szekhely">A hozzáadni kívánt székhely példány.</param>
        public void AddSzekhely(IRepository<Székhely> szekhelyrepo, Székhely szekhely)
        {
            try
            {
                szekhelyrepo.Create(szekhely);
            }
            catch (Exception)
            {
                Console.Clear();
                Console.WriteLine("A hozzáadást nem sikerült elvégezni, a megadott kulcs már foglalt vagy nem megfelelő formátumú!");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Telefon hozzáadásához szükséges metódus.
        /// </summary>
        /// <param name="telorepo">A metódushoz szükséges telefon repository.</param>
        /// <param name="telo">A hozzáadni kívánt telefon példány.</param>
        public void AddTelefon(IRepository<Telefonok> telorepo, Telefonok telo)
        {
            try
            {
                telorepo.Create(telo);
            }
            catch (Exception)
            {
                Console.Clear();
                Console.WriteLine("A hozzáadást nem sikerült elvégezni, a megadott kulcs már foglalt vagy nem megfelelő formátumú!");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Törléshez szökséges algoritmus.
        /// </summary>
        /// <param name="tableName">A tábla neve amiből törölni szeretnénk.</param>
        /// <param name="s">A törölni kívánt elem id-ja.</param>
        /// <param name="telorepo">A metódushoz szükséges telefon repository.</param>
        /// <param name="gyartorepo">A metódushoz szükséges gyártó repository.</param>
        /// <param name="szekhelyrepo">A metódushoz szükséges székhely repository.</param>
        public void Delete(TableNames tableName, string s, IRepository<Telefonok> telorepo, IRepository<Gyártó> gyartorepo, IRepository<Székhely> szekhelyrepo)
        {
            switch (tableName)
            {
                case TableNames.Gyártó:
                    try
                    {
                        gyartorepo.Delete(gyartorepo.GetByID(s));
                    }
                    catch (Exception)
                    {
                        Console.Clear();
                        Console.WriteLine("A törlést nem lehetett elvégezni kulcsfüggőség miatt");
                        Console.ReadKey();
                    }

                    break;
                case TableNames.Telefonok:
                    telorepo.Delete(telorepo.GetByID(s));
                    break;
                case TableNames.Székhely:
                    szekhelyrepo.Delete(szekhelyrepo.GetByID(s));
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Delete metódus.
        /// </summary>
        /// <param name="id">A törölni kívánt elem id-je</param>
        /// <returns>Sikeres volt-e a törlés.</returns>
        public bool DeleteGyarto(int id)
        {
            return this.gyRepo.Deleteforprog4(id);
        }

        /// <summary>
        /// Megadja egy string listában hogy az adott táblába való új elem hozzáadásához milyen adatokat szükséges megadni.
        /// </summary>
        /// <param name="tableName">Az adott tábla neve</param>
        /// <returns>A szükséges adatok listája</returns>
        public List<string> ElementRequirements(TableNames tableName)
        {
            List<string> valuesNeeded = new List<string>();
            switch (tableName)
            {
                case TableNames.Telefonok:
                    valuesNeeded.Add("A telefon modell neve:(Az adatbázisban már szereplő nevet ne adjon meg!)");
                    valuesNeeded.Add("A telefon kiadásának éve:");
                    valuesNeeded.Add("A telefon kamerájának felbontása:");
                    valuesNeeded.Add("A telefon operációs rendszere:");
                    valuesNeeded.Add("A telefon ram mérete:");
                    valuesNeeded.Add("A telefon tárhelyének mérete:");
                    valuesNeeded.Add("A telefon gyártójának id-je (Csak létező gyártó id-t adjon meg!)");
                    break;
                case TableNames.Gyártó:
                    valuesNeeded.Add("A gyártó id-ja: (Ne adjon meg a már meglévő gyártók idjával egyező id-t!)");
                    valuesNeeded.Add("A gyártó neve:");
                    valuesNeeded.Add("A gyártó éves forgalma:");
                    valuesNeeded.Add("Csak informatikai cikkekkel foglalkozik-e a gyártó? (True/False)");
                    valuesNeeded.Add("Milyen operációs rendszert használ a gyártó elsősorban?");
                    valuesNeeded.Add("A gyártó weblapjának címe:");
                    break;
                case TableNames.Székhely:
                    valuesNeeded.Add("A város neve:");
                    valuesNeeded.Add("Az ország neve:");
                    valuesNeeded.Add("A kontinens neve:");
                    valuesNeeded.Add("Egyedüli (monopol)-e a gyártó az adott országban? (True/False)");
                    valuesNeeded.Add("A székhely típusa:");
                    valuesNeeded.Add("A székhely id-ja: (Ne adjon meg a már meglévő gyártók idjával egyező id-t!)");
                    valuesNeeded.Add("A székhelyhez tartozó gyártó id-ja: (Csak olyan gyártó id-t adjon meg ami szerepel az adatbázisban!)");
                    break;
                default:
                    break;
            }

            return valuesNeeded;
        }

        /// <summary>
        /// Visszaadja a megadott gyártó telefonjainak modell nevét a táblából.
        /// </summary>
        /// <param name="telorep">A metódushoz szükséges telefon repository.</param>
        /// <param name="gyartoid">A választott gyártó id-je.</param>
        /// <returns>A gyártó telefonjainak modell neveinek listája.</returns>
        public List<string> AdottGyartoTelefonjai(IRepository<Telefonok> telorep, int gyartoid)
        {
            var q1 = (from x in telorep.Read()
                      where x.Gyártó_ID == gyartoid
                      select x.Modell_Név).ToList();
            return q1;
        }

        /// <summary>
        /// Egy adott évnél később kiadott telefonok listázása.
        /// </summary>
        /// <param name="telorep">A metódushoz szükséges telefon repository.</param>
        /// <param name="kiadas">A választott év.</param>
        /// <returns>A választott évnél később megjelent telefonok modellnevei egy stringben. Vagy egy üzenet, hogy nem található a megadottnál későbbi telefon a táblába.</returns>
        public string AdottEvnelKesobbiTelok(IRepository<Telefonok> telorep, int kiadas)
        {
            string vissza = string.Empty;
            var q2 = from x in telorep.Read()
                     where x.Kiadás_éve > kiadas
                     select x.Modell_Név;
            if (q2 == null)
            {
                vissza = "Nem található a táblában a megadott évszámnál később megjelent telefon!";
            }
            else
            {
                foreach (var item in q2)
                {
                    vissza = vissza + item + "\n";
                }
            }

            return vissza;
        }

        /// <summary>
        /// Megadott gyártó székhelyének országának és városának kikeresése.
        /// </summary>
        /// <param name="székhelyrep">A metódushoz szükséges székhely repository.</param>
        /// <param name="gyártóid">A választott gyártó id-je.</param>
        /// <returns>A választott gyártó országa és városa.</returns>
        public string AdottGyartoOrszagVaros(IRepository<Székhely> székhelyrep, int gyártóid)
        {
            string vissza = string.Empty;
            var q3 = (from x in székhelyrep.Read()
                      where x.Gyártó_ID == gyártóid
                      select x).First();
            vissza += q3.Ország + " " + q3.Város;

            return vissza;
        }

        /// <summary>
        /// A gyártó tábla kiolvasása.
        /// </summary>
        /// <param name="gyartorepo">A metódushoz szükséges gyártó repository.</param>
        /// <returns>A gyártók listája.</returns>
        public List<Gyártó> ReadGyartok(IRepository<Gyártó> gyartorepo)
        {
            return gyartorepo.Read();
        }

        /// <summary>
        /// Read metódus.
        /// </summary>
        /// <returns>Az adatbázisban található gyártók.</returns>
        public List<Gyártó> ReadGyartokforprog4()
        {
            return this.gyRepo.Read();
        }

        /// <summary>
        /// A telefon tábla kiolvasása.
        /// </summary>
        /// <param name="telorepo">A metódushoz szükséges telefon repository.</param>
        /// <returns>A telefonok listája.</returns>
        public List<Telefonok> ReadTelok(IRepository<Telefonok> telorepo)
        {
            return telorepo.Read();
        }

        /// <summary>
        /// A székhely tábla kiolvasása.
        /// </summary>
        /// <param name="szekhelyrepo">A metódushoz szükséges székhely repository.</param>
        /// <returns>A székhelyek listája.</returns>
        public List<Székhely> SzekhelyRead(IRepository<Székhely> szekhelyrepo)
        {
            return szekhelyrepo.Read();
        }

        /// <summary>
        /// A gyártó tábla egy elemének módosítása.
        /// </summary>
        /// <param name="gyartorepo">A metódushoz szükséges gyártó repository.</param>
        /// <param name="old">A módosítani kívánt gyártó.</param>
        /// <param name="updated">A módosított gyártó.</param>
        public void UpdateGyartok(IRepository<Gyártó> gyartorepo, Gyártó old, Gyártó updated)
        {
            gyartorepo.Update(old, updated);
        }

        /// <summary>
        /// Update metódus.
        /// </summary>
        /// <param name="id">Id</param>
        /// <param name="name">Név</param>
        /// <param name="forg">Éves forgalom</param>
        /// <param name="csakinf">Csak informatikai cikkeket gyárt-e a cég.</param>
        /// <param name="szoftver">Szoftver</param>
        /// <param name="weblap">Weblap</param>
        /// <returns>Sikeres volt-e az update.</returns>
        public bool UpdateGyartokforprog4(int id, string name, int forg, string csakinf, string szoftver, string weblap)
        {
            string s = id.ToString();
            Gyártó updated = new Gyártó();
            updated.Gyártó_ID = int.Parse(s);
            updated.Gyártó_Név = name;
            updated.Szoftver = szoftver;
            updated.Weblap = weblap;
            updated.Éves_forgalom = forg;
            updated.Csak_info = csakinf;

            return this.gyRepo.UpdateforProg4(this.GetGyartoforprog4(id), updated);
        }

        /// <summary>
        /// A székhely tábla egy elemének módosítása.
        /// </summary>
        /// <param name="szekhelyrepo">A metódushoz szükséges székhely repository.</param>
        /// <param name="old">A módosítani kívánt székhely.</param>
        /// <param name="updated">A módosított székhely.</param>
        public void UpdateSzekhely(IRepository<Székhely> szekhelyrepo, Székhely old, Székhely updated)
        {
            szekhelyrepo.Update(old, updated);
        }

        /// <summary>
        /// A telefonok tábla egy elemének módosítása.
        /// </summary>
        /// <param name="telorep">A metódushoz szükséges telefon repository.</param>
        /// <param name="old">A módosítani kívánt telefon.</param>
        /// <param name="updated">A módosított telefon.</param>
        public void UpdateTelok(IRepository<Telefonok> telorep, Telefonok old, Telefonok updated)
        {
            telorep.Update(old, updated);
        }

        /// <summary>
        /// Visszaadja a táblában már szereplő gyártók nevét és id-jét egy listába.
        /// </summary>
        /// <param name="gyartorep">A metódushoz szükséges gyártó repository.</param>
        /// <returns>A gyártók nevei és id-jei.</returns>
        public List<string> GyartokEsIdjuk(IRepository<Gyártó> gyartorep)
        {
            List<string> vissza = new List<string>();
            List<Gyártó> table = gyartorep.Read();
            foreach (var item in table)
            {
                Gyártó gy = item as Gyártó;
                vissza.Add($"Gyártónév: {gy.Gyártó_Név} Id-je: {gy.Gyártó_ID}");
            }

            return vissza;
        }

        /// <summary>
        /// Visszaadja a táblában már szereplő székhelyek városát és id-jét.
        /// </summary>
        /// <param name="szekhelyrepo">A metódushoz szükséges székhely repository.</param>
        /// <returns>A székhelyek városai és id-jei.</returns>
        public List<string> SzekhelyekEsIdjuk(IRepository<Székhely> szekhelyrepo)
        {
            List<string> vissza = new List<string>();
            List<Székhely> table = szekhelyrepo.Read();
            foreach (var item in table)
            {
                Székhely sz = item as Székhely;
                vissza.Add($"Székhely városa: {sz.Város} Id-je: {sz.Székhely_ID}");
            }

            return vissza;
        }

        /// <summary>
        /// Visszaadja a táblában már szereplő telefonok modell nevét és id-jét egy listába.
        /// </summary>
        /// <param name="telorep">A metódushoz szükséges telefon repository.</param>
        /// <returns>A telefonok modell nevei és id-jei.</returns>
        public List<string> Meglevotelefonok(IRepository<Telefonok> telorep)
        {
            List<string> vissza = new List<string>();
            List<Telefonok> table = telorep.Read();
            foreach (var item in table)
            {
                Telefonok telo = item as Telefonok;
                vissza.Add($"Modell neve: {telo.Modell_Név}");
            }

            return vissza;
        }

        /// <summary>
        /// Eldönti hogy egy adott elem benne van-e az adott táblában vagy sem.
        /// </summary>
        /// <param name="tableName">Az adott tábla neve.</param>
        /// <param name="s">Az adott elem id-ja.</param>
        /// <returns>Egy logikai érték, true ha az elem benne van a táblában, false ha nem.</returns>
        public bool Existsornot(TableNames tableName, string s)
        {
            switch (tableName)
            {
                case TableNames.Gyártó:
                    return this.gyRepo.GetByID(s) != null;
                case TableNames.Telefonok:
                    return this.tRepo.GetByID(s) != null;
                case TableNames.Székhely:
                    return this.szRepo.GetByID(s) != null;
                default:
                    break;
            }

            return false;
        }

        /// <summary>
        /// Vissza ad egy adott idvel rendelkező telefont.
        /// </summary>
        /// <param name="repo">A metódushoz szükséges telefon repository.</param>
        /// <param name="s">Az adott id.</param>
        /// <returns>Az adott idvel rendelkező telefon.</returns>
        public Telefonok GetTelok(IRepository<Telefonok> repo, string s)
        {
            return (from r in repo.Read()
                    where r.Modell_Név == s
                    select r).First();
        }

        /// <summary>
        /// Vissza ad egy adott idvel rendelkező gyártót.
        /// </summary>
        /// <param name="repo">A metódushoz szükséges gyártó repository.</param>
        /// <param name="s">Az adott id.</param>
        /// <returns>Az adott idvel rendelkező gyártó.</returns>
        public Gyártó GetGyartok(IRepository<Gyártó> repo, string s)
        {
            return (from r in repo.Read()
                    where r.Gyártó_ID == int.Parse(s)
                    select r).First();
        }

        /// <summary>
        /// Visszaad egy szártót id alapján.
        /// </summary>
        /// <param name="id">A keresett gyártó id-je.</param>
        /// <returns>A keresett gyártó.</returns>
        public Gyártó GetGyartoforprog4(int id)
        {
            string s = id.ToString();
            return this.gyRepo.GetByID(s);
        }

        /// <summary>
        /// Vissza ad egy adott idvel rendelkező székhelyet.
        /// </summary>
        /// <param name="repo">A metódushoz szükséges székhely repository.</param>
        /// <param name="s">Az adott id.</param>
        /// <returns>Az adott idvel rendelkező telefon.</returns>
        public Székhely GetSzekhely(IRepository<Székhely> repo, string s)
        {
            return (from r in repo.Read()
                    where r.Székhely_ID == int.Parse(s)
                    select r).First();
        }
    }
}
