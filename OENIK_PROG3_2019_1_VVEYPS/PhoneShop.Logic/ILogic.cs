﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhoneShop.Data;
    using PhoneShop.Repository;

    /// <summary>
    /// Az üzleti logika megvalósítandó metódusait leíró interface.
    /// </summary>
    internal interface ILogic
    {
        /// <summary>
        /// Megadja egy string listában hogy az adott táblába való új elem hozzáadásához milyen adatokat szükséges megadni.
        /// </summary>
        /// <param name="tableName">Az adott tábla neve</param>
        /// <returns>A szükséges adatok listája</returns>
        List<string> ElementRequirements(TableNames tableName);

        /// <summary>
        /// Telefon hozzáadásához szükséges metódus.
        /// </summary>
        /// <param name="telorepo">A metódushoz szükséges telefon repository.</param>
        /// <param name="telo">A hozzáadni kívánt telefon példány.</param>
        void AddTelefon(IRepository<Telefonok> telorepo, Telefonok telo);

        /// <summary>
        /// Gyártó hozzáadásához szükséges metódus.
        /// </summary>
        /// <param name="gyartorepo">A metódushoz szükséges gyártó repository.</param>
        /// <param name="gyarto">A hozzáadni kívánt gyártó példány.</param>
        void AddGyarto(IRepository<Gyártó> gyartorepo, Gyártó gyarto);

        /// <summary>
        /// Székhely hozzáadásához szükséges metódus.
        /// </summary>
        /// <param name="szekhelyrepo">A metódushoz szükséges székhely repository.</param>
        /// <param name="szekhely">A hozzáadni kívánt székhely példány.</param>
        void AddSzekhely(IRepository<Székhely> szekhelyrepo, Székhely szekhely);

        /// <summary>
        /// Törléshez szökséges algoritmus.
        /// </summary>
        /// <param name="tableName">A tábla neve amiből törölni szeretnénk.</param>
        /// <param name="s">A törölni kívánt elem id-ja.</param>
        /// <param name="telorepo">A metódushoz szükséges telefon repository.</param>
        /// <param name="gyartorepo">A metódushoz szükséges gyártó repository.</param>
        /// <param name="szekhelyrepo">A metódushoz szükséges székhely repository.</param>
        void Delete(TableNames tableName, string s, IRepository<Telefonok> telorepo, IRepository<Gyártó> gyartorepo, IRepository<Székhely> szekhelyrepo);

        /// <summary>
        /// A telefon tábla kiolvasása.
        /// </summary>
        /// <param name="telorepo">A metódushoz szükséges telefon repository.</param>
        /// <returns>A telefonok listája.</returns>
        List<Telefonok> ReadTelok(IRepository<Telefonok> telorepo);

        /// <summary>
        /// A gyártó tábla kiolvasása.
        /// </summary>
        /// <param name="gyartorepo">A metódushoz szükséges gyártó repository.</param>
        /// <returns>A gyártók listája.</returns>
        List<Gyártó> ReadGyartok(IRepository<Gyártó> gyartorepo);

        /// <summary>
        /// A székhely tábla kiolvasása.
        /// </summary>
        /// <param name="szekhelyrepo">A metódushoz szükséges székhely repository.</param>
        /// <returns>A székhelyek listája.</returns>
        List<Székhely> SzekhelyRead(IRepository<Székhely> szekhelyrepo);

        /// <summary>
        /// A telefonok tábla egy elemének módosítása.
        /// </summary>
        /// <param name="telorep">A metódushoz szükséges telefon repository.</param>
        /// <param name="old">A módosítani kívánt telefon.</param>
        /// <param name="updated">A módosított telefon.</param>
        void UpdateTelok(IRepository<Telefonok> telorep, Telefonok old, Telefonok updated);

        /// <summary>
        /// A gyártó tábla egy elemének módosítása.
        /// </summary>
        /// <param name="gyartorepo">A metódushoz szükséges gyártó repository.</param>
        /// <param name="old">A módosítani kívánt gyártó.</param>
        /// <param name="updated">A módosított gyártó.</param>
        void UpdateGyartok(IRepository<Gyártó> gyartorepo, Gyártó old, Gyártó updated);

        /// <summary>
        /// A székhely tábla egy elemének módosítása.
        /// </summary>
        /// <param name="szekhelyrepo">A metódushoz szükséges székhely repository.</param>
        /// <param name="old">A módosítani kívánt székhely.</param>
        /// <param name="updated">A módosított székhely.</param>
        void UpdateSzekhely(IRepository<Székhely> szekhelyrepo, Székhely old, Székhely updated);

        /// <summary>
        /// Visszaadja a megadott gyártó telefonjainak modell nevét a táblából.
        /// </summary>
        /// <param name="telorep">A metódushoz szükséges telefon repository.</param>
        /// <param name="gyartoid">A választott gyártó id-je.</param>
        /// <returns>A gyártó telefonjainak modell neveinek listája.</returns>
        List<string> AdottGyartoTelefonjai(IRepository<Telefonok> telorep, int gyartoid);

        /// <summary>
        /// Egy adott évnél később kiadott telefonok listázása.
        /// </summary>
        /// <param name="telorep">A metódushoz szükséges telefon repository.</param>
        /// <param name="kiadas">A választott év.</param>
        /// <returns>A választott évnél később megjelent telefonok modellnevei egy stringben. Vagy egy üzenet, hogy nem található a megadottnál későbbi telefon a táblába.</returns>
        string AdottEvnelKesobbiTelok(IRepository<Telefonok> telorep, int kiadas);

        /// <summary>
        /// Megadott gyártó székhelyének országának és városának kikeresése.
        /// </summary>
        /// <param name="székhelyrep">A metódushoz szükséges székhely repository.</param>
        /// <param name="gyártóid">A választott gyártó id-je.</param>
        /// <returns>A választott gyártó országa és városa.</returns>
        string AdottGyartoOrszagVaros(IRepository<Székhely> székhelyrep, int gyártóid);
    }
}
