﻿// <copyright file="GyártóRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhoneShop.Data;

    /// <summary>
    /// Az IRepository interfacet megvalósító Gyártó repository
    /// </summary>
    public class GyártóRepository : IRepository<Gyártó>
    {
        private readonly Database1Entities database;

        /// <summary>
        /// Initializes a new instance of the <see cref="GyártóRepository"/> class.
        /// </summary>
        /// <param name="database">Egy dbcontext példány</param>
        public GyártóRepository(Database1Entities database)
        {
            this.database = database;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GyártóRepository"/> class.
        /// </summary>
        public GyártóRepository()
        {
        }

        /// <summary>
        /// A create metódus.
        /// </summary>
        /// <param name="newdata">A hozzáadni kívánt elem.</param>
        public void Create(Gyártó newdata)
        {
            this.database.Gyártó.Add(newdata);
            this.database.SaveChanges();
        }

        /// <summary>
        /// A create metódus.
        /// </summary>
        /// <param name="newdata">A hozzáadni kívánt elem.</param>
        public void Createforprog4(Gyártó newdata)
        {
            this.database.Set<Gyártó>().Add(newdata);
            this.database.SaveChanges();
        }

        /// <summary>
        /// A Delete metódus.
        /// </summary>
        /// <param name="olddata">A törölni kívánt elem.</param>
        public void Delete(Gyártó olddata)
        {
            this.database.Gyártó.Remove(olddata);
            this.database.SaveChanges();
        }

        /// <summary>
        /// A Delete metódus.
        /// </summary>
        /// <param name="id">A törölni kívánt elem idje.</param>
        /// <returns>Sikeres volt-e a törlés.</returns>
        public bool Deleteforprog4(int id)
        {
            try
            {
            string s = id.ToString();
            Gyártó entity = this.GetByID(s);
            if (entity == null)
            {
                return false;
            }

            this.database.Set<Gyártó>().Remove(entity);
            this.database.SaveChanges();
            return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Az update metódus.
        /// </summary>
        /// <param name="old">A módosítani kívánt elem.</param>
        /// <param name="updated">A módosított elem.</param>
        public void Update(Gyártó old, Gyártó updated)
        {
            this.database.Gyártó.Remove(old);
            this.database.Gyártó.Add(updated);
            this.database.SaveChanges();
        }

        /// <summary>
        /// Az update metódus.
        /// </summary>
        /// <param name="old">A módosítani kívánt elem.</param>
        /// <param name="updated">A módosított elem.</param>
        /// <returns>Sikeres volt-e az update.</returns>
        public bool UpdateforProg4(Gyártó old, Gyártó updated)
        {
            Gyártó entity = this.GetByID(old.Gyártó_ID.ToString());
            if (entity == null)
            {
                return false;
            }

            entity.Gyártó_ID = updated.Gyártó_ID;
            entity.Gyártó_Név = updated.Gyártó_Név;
            entity.Csak_info = updated.Csak_info;
            entity.Éves_forgalom = updated.Éves_forgalom;
            entity.Weblap = updated.Weblap;
            entity.Szoftver = updated.Szoftver;
            this.database.SaveChanges();
            return true;
        }

        /// <summary>
        /// A read metódus.
        /// </summary>
        /// <returns>Gyártó típusú lista.</returns>
        public List<Gyártó> Read()
        {
            return this.database.Gyártó.ToList();
        }

        /// <summary>
        /// Egy adott idvel rendelkező gyártó visszaadása.
        /// </summary>
        /// <param name="s">Az adott id.</param>
        /// <returns>Az idvel rendelkező gyártó.</returns>
        public Gyártó GetByID(string s)
        {
            int id = int.Parse(s);
            return this.database.Gyártó.Single(x => x.Gyártó_ID == id);
        }
    }
}
