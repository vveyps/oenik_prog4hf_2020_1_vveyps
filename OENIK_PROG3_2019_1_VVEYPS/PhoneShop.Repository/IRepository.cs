﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>
namespace PhoneShop.Repository
#pragma warning restore SA1652 // Enable XML documentation output
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhoneShop.Data;

    /// <summary>
    /// A különböző reposytoriknak metódusokat előíró interface.
    /// </summary>
    /// <typeparam name="TEntity">TEntity típusú</typeparam>
    public interface IRepository<TEntity>
    {
        /// <summary>
        /// A create metódus.
        /// </summary>
        /// <param name="newdata">A hozzáadni kívánt elem.</param>
        void Create(TEntity newdata);

        /// <summary>
        /// A delete metódus.
        /// </summary>
        /// <param name="olddata">A törölni kívánt elem.</param>
        void Delete(TEntity olddata);

        /// <summary>
        /// Az update metódus
        /// </summary>
        /// <param name="old">A módosítani kívánt elem.</param>
        /// <param name="updated">A módosított elem.</param>
        void Update(TEntity old, TEntity updated);

        /// <summary>
        /// A read metódus.
        /// </summary>
        /// <returns>Egy TEntity típusú lista.</returns>
        List<TEntity> Read();

        /// <summary>
        /// Egy id alapján az elemet visszaadó metódus.
        /// </summary>
        /// <param name="s">A választott elem idje.</param>
        /// <returns>A válaszott elem.</returns>
        TEntity GetByID(string s);
    }
}
