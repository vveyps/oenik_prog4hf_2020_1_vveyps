﻿// <copyright file="SzékhelyRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhoneShop.Data;

    /// <summary>
    /// Az IRepository interfacet megvalósító székhely repository.
    /// </summary>
    public class SzékhelyRepository : IRepository<Székhely>
    {
        private readonly Database1Entities database = new Database1Entities();

        /// <summary>
        /// A create metódus.
        /// </summary>
        /// <param name="newdata">A hozzáadni kívánt elem.</param>
        public void Create(Székhely newdata)
        {
            this.database.Székhely.Add(newdata);
            this.database.SaveChanges();
        }

        /// <summary>
        /// A Delete metódus.
        /// </summary>
        /// <param name="olddata">A törölni kívánt elem.</param>
        public void Delete(Székhely olddata)
        {
            this.database.Székhely.Remove(olddata);
            this.database.SaveChanges();
        }

        /// <summary>
        /// Egy adott idvel rendelkező székhely visszaadása.
        /// </summary>
        /// <param name="s">Az adott id.</param>
        /// <returns>Az idvel rendelkező székhely.</returns>
        public Székhely GetByID(string s)
        {
            return this.database.Székhely.Single(x => x.Székhely_ID == int.Parse(s));
        }

        /// <summary>
        /// A read metódus.
        /// </summary>
        /// <returns>Székhely típusú lista.</returns>
        public List<Székhely> Read()
        {
            return this.database.Székhely.ToList();
        }

        /// <summary>
        /// Az update metódus.
        /// </summary>
        /// <param name="old">A módosítani kívánt elem.</param>
        /// <param name="updated">A módosított elem.</param>
        public void Update(Székhely old, Székhely updated)
        {
            this.database.Székhely.Remove(old);
            this.database.Székhely.Add(updated);
            this.database.SaveChanges();
        }
    }
}
