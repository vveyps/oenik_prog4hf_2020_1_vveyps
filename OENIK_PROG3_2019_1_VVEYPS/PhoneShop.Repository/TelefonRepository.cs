﻿// <copyright file="TelefonRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using PhoneShop.Data;

    /// <summary>
    /// Az IRepository interfacet megvalósító telefon repository
    /// </summary>
    public class TelefonRepository : IRepository<Telefonok>
    {
        private readonly Database1Entities database = new Database1Entities();

        /// <summary>
        /// A create metódus.
        /// </summary>
        /// <param name="newdata">A hozzáadni kívánt elem.</param>
        public void Create(Telefonok newdata)
        {
            this.database.Telefonok.Add(newdata);
            this.database.SaveChanges();
        }

        /// <summary>
        /// A Delete metódus.
        /// </summary>
        /// <param name="olddata">A törölni kívánt elem.</param>
        public void Delete(Telefonok olddata)
        {
            this.database.Telefonok.Remove(olddata);
            this.database.SaveChanges();
        }

        /// <summary>
        /// Egy adott idvel rendelkező telefon visszaadása.
        /// </summary>
        /// <param name="s">Az adott id.</param>
        /// <returns>Az idvel rendelkező telefon.</returns>
        public Telefonok GetByID(string s)
        {
            return this.database.Telefonok.Single(x => x.Modell_Név == s);
        }

        /// <summary>
        /// Az update metódus.
        /// </summary>
        /// <param name="old">A módosítani kívánt elem.</param>
        /// <param name="updated">A módosított elem.</param>
        public void Update(Telefonok old, Telefonok updated)
        {
            this.database.Telefonok.Remove(old);
            this.database.Telefonok.Add(updated);
            this.database.SaveChanges();
        }

        /// <summary>
        /// A read metódus.
        /// </summary>
        /// <returns>Telefonok típusú lista.</returns>
        public List<Telefonok> Read()
        {
            return this.database.Telefonok.ToList();
        }
    }
}
