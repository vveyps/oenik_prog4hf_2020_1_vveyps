﻿using AutoMapper;
using PhoneShop.Data;
using PhoneShop.Repository;
using PhoneShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PhoneShop.Web.Controllers
{
    public class BrandsApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        PhoneShop.Logic.BusinessLogic logic;
        IMapper mapper;
        public BrandsApiController()
        {
            Database1Entities db = new Database1Entities();
            GyártóRepository brandRepo = new GyártóRepository(db);
            logic = new Logic.BusinessLogic(brandRepo);
            mapper = MapperFactory.CreateMapper();
        }

        // GET api/BrandsApi
        [ActionName("all")]
        [HttpGet]
        //GET api/BrandsApi/all
        public IEnumerable<Models.Brand> GetAll()
        {
            var brands = logic.ReadGyartokforprog4();
            return mapper.Map<IList<Data.Gyártó>, List<Models.Brand>>(brands);
        }

        // GET api/BrandsApi/del/10
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneBrand(int id)
        {
            bool success = logic.DeleteGyarto(id);
            return new ApiResult() { OperationResult = success };
        }

        // GET api/BrandsApi/add + car
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneBrand(Brand brand)
        {
            logic.AddGyartoForProg4(brand.Name, brand.Forgalom, brand.Csakinf, brand.Szoftver, brand.Weblap);
            return new ApiResult() { OperationResult = true };
        }

        // GET api/BrandsApi/del/10
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneBrand(Brand brand)
        {
            bool success = logic.UpdateGyartokforprog4(brand.Id, brand.Name, brand.Forgalom, brand.Csakinf, brand.Szoftver, brand.Weblap);
            return new ApiResult() { OperationResult = success };
        }
    }
}
