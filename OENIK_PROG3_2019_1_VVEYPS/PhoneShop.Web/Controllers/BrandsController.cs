﻿using AutoMapper;
using PhoneShop.Data;
using PhoneShop.Repository;
using PhoneShop.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PhoneShop.Web.Controllers
{
    public class BrandsController : Controller
    {
        PhoneShop.Logic.BusinessLogic logic;
        IMapper mapper;
        BrandsViewModel model;
        

        public BrandsController()
        {
            Database1Entities db = new Database1Entities();
            GyártóRepository brandRepo = new GyártóRepository(db);
            logic = new Logic.BusinessLogic(brandRepo);
            mapper = MapperFactory.CreateMapper();
            model = new BrandsViewModel();
            model.EditedBrand = new Brand();

            var brands = logic.ReadGyartokforprog4();
            model.ListOfBrands = mapper.Map<IList<Data.Gyártó>, List<Models.Brand>>(brands);
        }

        private Brand GetOneBrand(int id)
        {
            Gyártó oneBrand = logic.GetGyartoforprog4(id);
            return mapper.Map<Gyártó, Brand>(oneBrand);
        }

        // GET: Brands
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("BrandIndex", model);
        }

        // GET: Brands/Details/5
        public ActionResult Details(int id)
        {
            return View("BrandDetails", GetOneBrand(id));
        }

        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete Fail";
            if (logic.DeleteGyarto(id))
            {
                TempData["editResult"] = "Delete OK";
            }
            return RedirectToAction("Index");
        }
        
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            model.EditedBrand = GetOneBrand(id);
            return View("BrandIndex", model);
        }

        [HttpPost]
        public ActionResult Edit(Brand brand, string editAction)
        {
            if (ModelState.IsValid && brand != null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction=="AddNew")
                {
                    logic.AddGyartoForProg4(brand.Name, brand.Forgalom, brand.Csakinf, brand.Szoftver, brand.Weblap);
                }
                else
                {
                    if (!logic.UpdateGyartokforprog4(brand.Id, brand.Name, brand.Forgalom, brand.Csakinf, brand.Szoftver, brand.Weblap))
                    {
                        TempData["editResult"] = "Edit FAIL";
                    }
                }
                return RedirectToAction("Index");
            }
            else
            {
                ViewData["editAction"] = "Edit";
                model.EditedBrand = brand;
                return View("BrandIndex", model);
            }

        }

    }
}
