﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PhoneShop.Web.Models
{
    public class Brand
    {
        [Display(Name = "Brand id")]
        [Required]
        public int Id { get; set; }
        [Display(Name = "Brand name")]
        [Required]
        public string Name { get; set; }
        [Display(Name = "Yearly income (usd)")]
        [Required]
        public int Forgalom { get; set; }
        [Display(Name = "Producing only smart devices")]
        [Required]
        public string Csakinf { get; set; }
        [Display(Name = "Software used by the brand")]
        [Required]
        public string Szoftver { get; set; }
        [Display(Name = "Web url")]
        [Required]
        public string Weblap { get; set; }
    }
}