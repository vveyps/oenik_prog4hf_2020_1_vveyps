﻿using PhoneShop.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoneShop.Web.Models
{
    public class BrandsViewModel
    {
        public Brand EditedBrand { get; set; }
        public List<Brand> ListOfBrands { get; set; }
    }
}