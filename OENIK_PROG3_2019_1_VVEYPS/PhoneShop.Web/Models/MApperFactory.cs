﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PhoneShop.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg => 
            {
                cfg.CreateMap<PhoneShop.Data.Gyártó, PhoneShop.Web.Models.Brand>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.Gyártó_ID)).
                ForMember(dest => dest.Name, map => map.MapFrom(src => src.Gyártó_Név)).
                ForMember(dest => dest.Forgalom, map => map.MapFrom(src => src.Éves_forgalom)).
                ForMember(dest => dest.Csakinf, map => map.MapFrom(src => src.Csak_info)).
                ForMember(dest => dest.Szoftver, map => map.MapFrom(src => src.Szoftver)).
                ForMember(dest => dest.Weblap, map => map.MapFrom(src => src.Weblap));
            });

            return config.CreateMapper();
        }
    }
}