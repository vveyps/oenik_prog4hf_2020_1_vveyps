﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PhoneShop.Wpf
{
    class BrandVM : ObservableObject
    {
        private int id;
        private string name;
        private string csakinf;
        private int forgalom;
        private string szoftver;
        private string weblap;

        public string Weblap
        {
            get { return weblap; }
            set { Set(ref weblap, value); }
        }


        public string Szoftver
        {
            get { return szoftver; }
            set { Set(ref szoftver, value); }
        }


        public int Forgalom
        {
            get { return forgalom; }
            set { Set(ref forgalom, value); }
        }


        public string Csakinf
        {
            get { return csakinf; }
            set { Set(ref csakinf, value); }
        }

        public string Name
        {
            get { return name; }
            set { Set(ref name, value); }
        }


        public int Id
        {
            get { return id; }
            set { Set(ref id, value); }
        }

        public void CopyFrom(BrandVM other)
        {
            if (other==null)
            {
                return;
            }
            this.Id = other.id;
            this.Name = other.name;
            this.Csakinf = other.csakinf;
            this.Forgalom = other.forgalom;
            this.Szoftver = other.szoftver;
            this.Weblap = other.weblap;
        }
    }
}
