﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace PhoneShop.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:55078/api/BrandsApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "BrandResult");
        }

        public List<BrandVM> ApiGetBrands()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<BrandVM>>(json);
            return list;
        }

        public void ApiDelCar(BrandVM brand)
        {
            bool success = false;
            if (brand != null)
            {
                string json = client.GetStringAsync(url + "del/" + brand.Id.ToString()).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }

        bool ApiEditBrand(BrandVM brand, bool isEditing)
        {
            if (brand == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(BrandVM.Id), brand.Id.ToString());
            postData.Add(nameof(BrandVM.Name),brand.Name);
            postData.Add(nameof(BrandVM.Forgalom), brand.Forgalom.ToString());
            postData.Add(nameof(BrandVM.Csakinf), brand.Csakinf);
            postData.Add(nameof(BrandVM.Szoftver), brand.Szoftver);
            postData.Add(nameof(BrandVM.Weblap), brand.Weblap);
            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }

        public void EditBrand(BrandVM brand, Func<BrandVM,bool> editor)
        {
            BrandVM clone = new BrandVM();
            if (brand != null) clone.CopyFrom(brand);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (brand != null) success = ApiEditBrand(clone, true);
                else success = ApiEditBrand(clone, false);
            }
            SendMessage(success == true);
        }
    }
}
