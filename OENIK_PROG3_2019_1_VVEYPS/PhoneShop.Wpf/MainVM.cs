﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace PhoneShop.Wpf
{
    class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private BrandVM selectedBrand;
        private ObservableCollection<BrandVM> allBrands;

        public ObservableCollection<BrandVM> AllBrands
        {
            get { return allBrands; }
            set { Set(ref allBrands, value); }
        }
        public BrandVM SelectedBrand
        {
            get { return selectedBrand; }
            set { Set(ref selectedBrand, value); }
        }

        public ICommand AddCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand LoadCmd { get; private set; }

        public Func<BrandVM, bool> EditorFunc { get; set; }

        public MainVM()
        {
            logic = new MainLogic();
            LoadCmd = new RelayCommand(() => AllBrands = new ObservableCollection<BrandVM>(logic.ApiGetBrands()));
            DelCmd = new RelayCommand(() => logic.ApiDelCar(selectedBrand));
            AddCmd = new RelayCommand(() => logic.EditBrand(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditBrand(selectedBrand, EditorFunc));
        }
    }
}
