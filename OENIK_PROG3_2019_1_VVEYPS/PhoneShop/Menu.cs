﻿// <copyright file="Menu.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using PhoneShop.Data;
    using PhoneShop.Logic;
    using PhoneShop.Repository;

    /// <summary>
    /// A menüt megvalósító osztály.
    /// </summary>
    internal class Menu
    {
        private readonly BusinessLogic logic;
        private List<string> table;
        private TelefonRepository teloRepo;
        private GyártóRepository gyartoRepo;
        private SzékhelyRepository szekhelyRepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="Menu"/> class.
        /// A menü osztály konstruktora.
        /// </summary>
        public Menu()
        {
            this.logic = new BusinessLogic();
            this.teloRepo = new TelefonRepository();
            this.gyartoRepo = new GyártóRepository();
            this.szekhelyRepo = new SzékhelyRepository();
            this.table = new List<string>();
        }

        /// <summary>
        /// A főmenüt megvalósító metódus. Ez hívja meg továbbá a választott opciókhoz tartozó metódusokat is.
        /// </summary>
        public void MainMenu()
        {
            bool shoutdown = false;
            while (!shoutdown)
            {
                Console.Clear();
                this.table.Clear();
                Console.Title = "Főmenü";
                Console.WriteLine("Menü\n" +
                    "Kérem nyomja meg a kívánt funkció eléréséhez tartozó gombot!\n" +
                    "1 - Táblák és elemeik listázása.\n" +
                    "2 - Választott táblához elem hozzáadása.\n" +
                    "3 - Választott tábla választott elemének törlése.\n" +
                    "4 - Választott tábla választott elemének módosítása.\n" +
                    "5 - Egy a felhasználó által megadott gyártó telefonjainak lekérdezése.\n" +
                    "6 - Egy a felhasználó által megadott évnél később megjelent telefonok lekérdezése.\n" +
                    "7 - Egy a felhasználó által megadott gyártó országa, és városa.\n" +
                    "8 - A telefonok megvásárlásához elérhető webshopok listája.\n" +
                    "9 - Kilépés.");
                ConsoleKeyInfo userInput = Console.ReadKey();
                if (int.TryParse(userInput.KeyChar.ToString(), out int choosenOption))
                {
                    choosenOption = int.Parse(userInput.KeyChar.ToString());
                }

                switch (choosenOption)
                {
                    case 1:
                        Console.Clear();
                        Console.WriteLine("Melyik táblát kívánja beolvasni?");
                        int checkIfNoChoice = this.TableSelectMenu();
                        if (checkIfNoChoice != 0)
                        {
                            Console.Clear();
                            this.ToConsole(this.table);
                            Console.WriteLine("Főmenübe visszalépéshez nyomja meg bármelyik gombot!");
                            Console.ReadKey();
                        }

                        break;
                    case 2:
                        Console.Clear();
                        this.Create();
                        Console.WriteLine("Főmenübe visszalépéshez nyomja meg bármelyik gombot!");
                        Console.ReadKey();
                        break;
                    case 3:
                        Console.Clear();
                        this.Delete();
                        break;
                    case 4:
                        Console.Clear();
                        Console.WriteLine("Melyik táblát kívánja beolvasni?");
                        int table = this.TableSelectMenu();
                        switch (table)
                        {
                            case 1:
                                this.UpdateTelefonok();
                                break;
                            case 2:
                                this.UpdateGyartok();
                                break;
                            case 3:
                                this.UpdateSzekhelyek();
                                break;
                            default:
                                break;
                        }

                        break;
                    case 5:
                        Console.Clear();
                        Console.WriteLine("Kérem adja meg a választott gyártó id-ját!");
                        int id = int.Parse(Console.ReadLine());
                        Console.Clear();
                        this.ToConsole(this.logic.AdottGyartoTelefonjai(this.teloRepo, id));
                        Console.ReadKey();
                        break;
                    case 6:
                        Console.Clear();
                        Console.WriteLine("Kérem adjon meg egy évszámot!");
                        int evszam = int.Parse(Console.ReadLine());
                        Console.Clear();
                        Console.WriteLine(this.logic.AdottEvnelKesobbiTelok(this.teloRepo, evszam));
                        Console.ReadKey();
                        break;
                    case 7:
                        Console.Clear();
                        Console.WriteLine("Kérem adja meg a választott gyártó id-ját!");
                        int id2 = int.Parse(Console.ReadLine());
                        Console.Clear();
                        Console.WriteLine(this.logic.AdottGyartoOrszagVaros(this.szekhelyRepo, id2));
                        Console.ReadKey();
                        break;
                    case 8:
                        Console.Clear();
                        using (var client = new WebClient())
                        {
                            byte[] xml_text = client.DownloadData($"http://localhost:8080/OENIK_PROG3_2019_1_VVEYPS/XML");
                            string xml = Encoding.ASCII.GetString(xml_text);
                            Console.WriteLine(xml);
                        }

                        Console.WriteLine("A főmenübe visszalépéshez nyomja meg bármelyik billentyűt!");
                            Console.ReadKey();
                        break;
                    case 9:
                        shoutdown = true;
                        break;
                    default:
                        Console.Clear();
                        Console.WriteLine("A választott gomb nincsen a lehetőségek között!");
                        Console.WriteLine("Főmenübe visszalépéshez nyomja meg bármelyik billentyűt!");
                        Console.ReadKey();
                        break;
                }
            }
        }

        private int TableSelectMenu()
        {
            Console.Title = "Tábla választó menü";
            Console.WriteLine("Kérem adja meg a választott táblát!\n" +
                "1: Telefonok \n" +
                "2: Gyártók \n" +
                "3: Székhelyek\n" +
                "Visszalépéshez nyomja meg bármelyik gombot!");
            ConsoleKeyInfo userInput = Console.ReadKey();
            if (int.TryParse(userInput.KeyChar.ToString(), out int choosenOption))
            {
                choosenOption = int.Parse(userInput.KeyChar.ToString());
            }

            if (this.table.Count != 0)
            {
                this.table = new List<string>();
            }

            switch (choosenOption)
            {
                case 1:
                    foreach (var p in this.teloRepo.Read())
                    {
                        this.table.Add(p.ToString());
                    }

                    break;
                case 2:
                    foreach (var t in this.gyartoRepo.Read())
                    {
                        this.table.Add(t.ToString());
                    }

                    break;
                case 3:
                    foreach (var c in this.szekhelyRepo.Read())
                    {
                        this.table.Add(c.ToString());
                    }

                    break;
                default:
                    choosenOption = 0;
                    break;
            }

            return choosenOption;
        }

        private void ToConsole(List<string> list)
        {
            if (list.Count != 0)
            {
                foreach (var item in list)
                {
                    Console.WriteLine(item);
                }
            }
        }

        private void Create()
        {
            int selectedTable = this.TableSelectMenu();
            if (selectedTable != 0)
            {
                List<string> ujAdathoz = new List<string>();
                Console.Clear();
                switch (selectedTable)
                {
                    case 1:
                        ujAdathoz = this.logic.ElementRequirements(TableNames.Telefonok);
                        Telefonok t = new Telefonok();
                        Console.WriteLine(ujAdathoz[0]);
                        t.Modell_Név = Console.ReadLine();
                        Console.WriteLine(ujAdathoz[1]);
                        t.Kiadás_éve = int.Parse(Console.ReadLine());
                        Console.WriteLine(ujAdathoz[2]);
                        t.Kamera = int.Parse(Console.ReadLine());
                        Console.WriteLine(ujAdathoz[3]);
                        t.Operációs_rendszer = Console.ReadLine();
                        Console.WriteLine(ujAdathoz[4]);
                        t.Ram = int.Parse(Console.ReadLine());
                        Console.WriteLine(ujAdathoz[5]);
                        t.Tárhely = int.Parse(Console.ReadLine());
                        Console.WriteLine(ujAdathoz[6]);
                        Console.WriteLine("A táblában szereplő gyártók és idjük:");
                        this.ToConsole(this.logic.GyartokEsIdjuk(this.gyartoRepo));
                        t.Gyártó_ID = int.Parse(Console.ReadLine());

                        this.logic.AddTelefon(this.teloRepo, t);
                        break;
                    case 2:
                        ujAdathoz = this.logic.ElementRequirements(TableNames.Gyártó);
                        Gyártó gy = new Gyártó();
                        Console.WriteLine(ujAdathoz[0]);
                        Console.WriteLine("A táblában szereplő gyártók és idjük:");
                        this.ToConsole(this.logic.GyartokEsIdjuk(this.gyartoRepo));
                        gy.Gyártó_ID = int.Parse(Console.ReadLine());
                        Console.WriteLine(ujAdathoz[1]);
                        gy.Gyártó_Név = Console.ReadLine();
                        Console.WriteLine(ujAdathoz[2]);
                        gy.Éves_forgalom = int.Parse(Console.ReadLine());
                        Console.WriteLine(ujAdathoz[3]);
                        gy.Csak_info = Console.ReadLine();
                        Console.WriteLine(ujAdathoz[4]);
                        gy.Szoftver = Console.ReadLine();
                        Console.WriteLine(ujAdathoz[5]);
                        gy.Weblap = Console.ReadLine();

                        this.logic.AddGyarto(this.gyartoRepo, gy);
                        break;
                    case 3:
                        ujAdathoz = this.logic.ElementRequirements(TableNames.Székhely);
                        Székhely sz = new Székhely();
                        Console.WriteLine(ujAdathoz[0]);
                        sz.Város = Console.ReadLine();
                        Console.WriteLine(ujAdathoz[1]);
                        sz.Ország = Console.ReadLine();
                        Console.WriteLine(ujAdathoz[2]);
                        sz.Kontinens = Console.ReadLine();
                        Console.WriteLine(ujAdathoz[3]);
                        sz.Monopol = Console.ReadLine();
                        Console.WriteLine(ujAdathoz[4]);
                        sz.Székhely_típus = Console.ReadLine();
                        Console.WriteLine(ujAdathoz[5]);
                        sz.Székhely_ID = int.Parse(Console.ReadLine());
                        Console.WriteLine(ujAdathoz[6]);
                        Console.WriteLine("A táblában szereplő gyártók és idjük:");
                        this.ToConsole(this.logic.GyartokEsIdjuk(this.gyartoRepo));
                        sz.Gyártó_ID = int.Parse(Console.ReadLine());

                        this.logic.AddSzekhely(this.szekhelyRepo, sz);
                        break;
                    default:
                        break;
                }
            }
        }

        private void Delete()
        {
            Console.Clear();
            Console.WriteLine("Melyik táblából kíván törölni?");
            int selectedmenu = this.TableSelectMenu();
            Console.Clear();
            this.ToConsole(this.table);
            Console.WriteLine("Írja be a törölni kívánt elem id-ját és nyomjon entert (A telefonok tábla id-ja a modell neve!)");
            string id = Console.ReadLine();

                switch (selectedmenu)
                {
                    case 1:
                        if (this.logic.Existsornot(TableNames.Telefonok, id))
                        {
                            this.logic.Delete(TableNames.Telefonok, id, this.teloRepo, null, null);
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Nem létező id-t adott meg! \n" + "Nyomjon meg egy gombot a főmenübe való visszalépéshez!");
                            Console.ReadKey();
                        }

                        break;
                    case 2:
                        if (this.logic.Existsornot(TableNames.Gyártó, id))
                        {
                            this.logic.Delete(TableNames.Gyártó, id, null, this.gyartoRepo, null);
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Nem létező id-t adott meg! \n" + "Nyomjon meg egy gombot a főmenübe való visszalépéshez!");
                            Console.ReadKey();
                        }

                        break;
                    case 3:
                        if (this.logic.Existsornot(TableNames.Székhely, id))
                        {
                            this.logic.Delete(TableNames.Székhely, id, null, null, this.szekhelyRepo);
                        }
                        else
                        {
                            Console.Clear();
                            Console.WriteLine("Nem létező id-t adott meg! \n" + "Nyomjon meg egy gombot a főmenübe való visszalépéshez!");
                            Console.ReadKey();
                        }

                        break;
                    default:
                        break;
                }
        }

        private void UpdateTelefonok()
        {
            Console.Clear();
            this.ToConsole(this.table);
            Console.WriteLine("Adja meg a módosítani kívánt elem id-jét (Telefonok tábla esetén a modell nevét)");
            string id = Console.ReadLine();
            if (this.logic.Existsornot(TableNames.Telefonok, id))
            {
                Telefonok t = this.logic.GetTelok(this.teloRepo, id);
                Telefonok newt = t;
                Console.WriteLine("Válassza ki melyik adatot szeretné módosítani!");
                Console.WriteLine("A telefon modell nevét csak akkor módosíthatja, ha az új modellnév még nem szerepel a táblában.");
                Console.WriteLine("A telefon gyártó id-ját csak akkor módosíthatja, ha a megadni kívánt id szerepel a gyártók táblában.");
                Console.WriteLine($"1 - A telefon modell neve: {t.Modell_Név}");
                Console.WriteLine($"2 - gyártó id-ja: {t.Gyártó_ID}");
                Console.WriteLine($"3 - A telefon kiadási éve: {t.Kiadás_éve}");
                Console.WriteLine($"4 - A telefon kamerájának felbontása: {t.Kamera}");
                Console.WriteLine($"5 - A telefon operációs rendszere: {t.Operációs_rendszer}");
                Console.WriteLine($"6 - A telefon tárhelyének mérete: {t.Tárhely}");
                Console.WriteLine($"7 - A telefon ramjának mérete: {t.Ram}");
                int selected = int.Parse(Console.ReadLine());
                switch (selected)
                {
                    case 1:
                        Console.WriteLine("Adja meg az új modell nevet!");
                        newt.Modell_Név = Console.ReadLine();
                        break;
                    case 2:
                        Console.WriteLine("A táblában szereplő gyártók és idjük:");
                        this.ToConsole(this.logic.GyartokEsIdjuk(this.gyartoRepo));
                        Console.WriteLine("Adja meg az új gyártó idt!");
                        newt.Gyártó_ID = int.Parse(Console.ReadLine());
                        break;
                    case 3:
                        Console.WriteLine("Adja meg az új kiadási évet!");
                        newt.Kiadás_éve = int.Parse(Console.ReadLine());
                        break;
                    case 4:
                        Console.WriteLine("Adja meg a kamera új felbontását!");
                        newt.Kamera = int.Parse(Console.ReadLine());
                        break;
                    case 5:
                        Console.WriteLine("Adja meg az új operációs rendszert!");
                        newt.Operációs_rendszer = Console.ReadLine();
                        break;
                    case 6:
                        Console.WriteLine("Adja meg a tárhely új méretét!");
                        newt.Tárhely = int.Parse(Console.ReadLine());
                        break;
                    case 7:
                        Console.WriteLine("Adja meg a ram új méretét!");
                        newt.Ram = int.Parse(Console.ReadLine());
                        break;
                    default:
                        break;
                }

                this.logic.UpdateTelok(this.teloRepo, t, newt);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Nem létező id-t adott meg! \n" + "Nyomjon meg egy gombot a főmenübe való visszalépéshez!");
                Console.ReadKey();
            }
        }

        private void UpdateGyartok()
        {
            Console.Clear();
            this.ToConsole(this.table);
            Console.WriteLine("Adja meg a módosítani kívánt elem id-jét");
            string id = Console.ReadLine();
            if (this.logic.Existsornot(TableNames.Gyártó, id))
            {
                Gyártó gy = this.logic.GetGyartok(this.gyartoRepo, id);
                Gyártó newgy = gy;
                Console.WriteLine("Válassza ki melyik adatot szeretné módosítani!");
                Console.WriteLine("A gyártó id-ját nem módosíthatja!");
                Console.WriteLine($"1 - A gyártó neve: {gy.Gyártó_Név}");
                Console.WriteLine($"2 - A gyártó éves forgalma: {gy.Éves_forgalom}");
                Console.WriteLine($"3 - A gyártó által használt operációs rendszer: {gy.Szoftver}");
                Console.WriteLine($"4 - A gyártó weblapja: {gy.Weblap}");
                Console.WriteLine($"5 - Csak informatikai cikkekkel foglalkozik-e a gyártó? (True/False): {gy.Csak_info}");
                int selected = int.Parse(Console.ReadLine());
                switch (selected)
                {
                    case 1:
                        Console.WriteLine("Adja meg a gyártó új nevét!");
                        newgy.Gyártó_Név = Console.ReadLine();
                        break;
                    case 2:
                        Console.WriteLine("Adja meg a gyártó új éves forgalmát!");
                        newgy.Éves_forgalom = int.Parse(Console.ReadLine());
                        break;
                    case 3:
                        Console.WriteLine("Adja meg a gyártó új operációs rendszerét!");
                        newgy.Szoftver = Console.ReadLine();
                        break;
                    case 4:
                        Console.WriteLine("Adja meg a gyártó új weblapját!");
                        newgy.Weblap = Console.ReadLine();
                        break;
                    case 5:
                        Console.WriteLine("Adja meg, hogy csak informatikai cikkekkel foglalkozik-e a gyártó!");
                        newgy.Csak_info = Console.ReadLine();
                        break;
                    default:
                        break;
                }

                this.logic.UpdateGyartok(this.gyartoRepo, gy, newgy);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Nem létező id-t adott meg! \n" + "Nyomjon meg egy gombot a főmenübe való visszalépéshez!");
                Console.ReadKey();
            }
        }

        private void UpdateSzekhelyek()
        {
            Console.Clear();
            this.ToConsole(this.table);
            Console.WriteLine("Adja meg a módosítani kívánt elem id-jét");
            string id = Console.ReadLine();
            if (this.logic.Existsornot(TableNames.Székhely, id))
            {
                Székhely sz = this.logic.GetSzekhely(this.szekhelyRepo, id);
                Székhely newsz = sz;
                Console.WriteLine("Válassza ki melyik adatot szeretné módosítani!");
                Console.WriteLine("A székhely id-ját csak akkor módosíthatja, ha az új id még nem szerepel a táblában!");
                Console.WriteLine("A telefon gyártó id-ját csak akkor módosíthatja, ha a megadni kívánt id szerepel a gyártók táblában.");
                Console.WriteLine("A gyártó id-ját nem módosíthatja!");
                Console.WriteLine($"1 - A székhely városa: {sz.Város}");
                Console.WriteLine($"2 - A székhely országa: {sz.Ország}");
                Console.WriteLine($"3 - A székhely kontinense: {sz.Kontinens}");
                Console.WriteLine($"4 - A székhely típusa: {sz.Székhely_típus}");
                Console.WriteLine($"5 - Egyedüli (monopol) helyzetben van-e a gyártó az adott országban? (True/False): {sz.Monopol}");
                Console.WriteLine($"6 - A székhely id-ja: {sz.Székhely_ID}");
                Console.WriteLine($"7 - A székhely gyártójának id-ja: {sz.Gyártó_ID}");
                int selected = int.Parse(Console.ReadLine());
                switch (selected)
                {
                    case 1:
                        Console.WriteLine("Adja meg a székhely új városát!");
                        newsz.Város = Console.ReadLine();
                        break;
                    case 2:
                        Console.WriteLine("Adja meg a székhely új országát!");
                        newsz.Ország = Console.ReadLine();
                        break;
                    case 3:
                        Console.WriteLine("Adja meg a székhely új kontinensét!");
                        newsz.Kontinens = Console.ReadLine();
                        break;
                    case 4:
                        Console.WriteLine("Adja meg a székhely új típusát!");
                        newsz.Székhely_típus = Console.ReadLine();
                        break;
                    case 5:
                        Console.WriteLine("Adja meg, hogy monopol helyzetben van-e a gyártó az adott országban!");
                        newsz.Monopol = Console.ReadLine();
                        break;
                    case 6:
                        Console.WriteLine("Adja meg a székhely új id-jét!");
                        newsz.Székhely_ID = int.Parse(Console.ReadLine());
                        break;
                    case 7:
                        Console.WriteLine("A táblában szereplő gyártók és idjük:");
                        this.ToConsole(this.logic.GyartokEsIdjuk(this.gyartoRepo));
                        Console.WriteLine("Adja meg a székhely új gyártó id-jét!");
                        newsz.Gyártó_ID = int.Parse(Console.ReadLine());
                        break;
                    default:
                        break;
                }

                this.logic.UpdateSzekhely(this.szekhelyRepo, sz, newsz);
            }
            else
            {
                Console.Clear();
                Console.WriteLine("Nem létező id-t adott meg! \n" + "Nyomjon meg egy gombot a főmenübe való visszalépéshez!");
                Console.ReadKey();
            }
        }
    }
}