﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace PhoneShop
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    /// <summary>
    /// A program osztály.
    /// </summary>
    internal class Program
    {
        private static void Main(string[] args)
        {
            Menu menu = new Menu();
            menu.MainMenu();
        }
    }
}
